FROM ligo/lalsuite-dev:el7
RUN git clone https://git.ligo.org/lscsoft/lalsuite-archive.git /tmp/src && pushd /tmp/src && git checkout lalsuite-v6.48 && ./00boot && ./configure --enable-swig-python --prefix=/usr && make -j && make install && popd && rm -rf /tmp/src
RUN git clone https://github.com/farr/skyarea.git /tmp/src && pushd /tmp/src && git checkout v0.3.2 && python setup.py install && popd && rm -rf /tmp/src
